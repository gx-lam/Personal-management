package com.nbclass.service;

import com.nbclass.example.AttendanceExample;
import com.nbclass.model.Attendance;

import java.util.List;

public interface AttendanceService {
    /**
     * 请假申请
     * @param attendance
     * @return
     */
    int insert(Attendance attendance);

    /**
     * 根据attendanceId撤回请假申请
     * @param example
     * @return
     */
    int deleteByAttendanceId(AttendanceExample example);

    /**
     * 根据Attendance对象查询请假列表(startTime,endTime,attendanceType,status)
     * @param attendance
     * @return
     */
    List<Attendance> selectByAttendance(Attendance attendance);

    /**
     * 根据attendanceId查询请假列表
     * @param example
     * @return
     */
    List<Attendance> findByAttendanceId(AttendanceExample example);

    /**
     * 根据userId查询请假列表
     * @param userId
     * @return
     */
    List<Attendance> findByUserId(String userId);

    /**
     * 根据userId查询请假对象
     * @param userId
     * @return
     */
    Attendance selectByUserId(String userId);

    /**
     * 根据approverId修改status
     * @param attendance
     * @return
     */
    int updateByExample(Attendance attendance);


}
