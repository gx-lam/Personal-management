package com.nbclass.service.impl;

import com.nbclass.example.AttendanceExample;
import com.nbclass.mapper.AttendanceMapper;
import com.nbclass.model.Attendance;
import com.nbclass.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceMapper attendanceMapper;


    @Override
    public int insert(Attendance attendance) {
        attendanceMapper.insert(attendance);
        return 0;
    }

    @Override
    public int deleteByAttendanceId(AttendanceExample example) {
        attendanceMapper.deleteByExample(example);
        return 0;
    }

    @Override
    public List<Attendance> selectByAttendance(Attendance attendance) {
        return attendanceMapper.selectByAttendance(attendance);
    }

    @Override
    public List<Attendance> findByAttendanceId(AttendanceExample example) {
        return attendanceMapper.selectByExample(example);
    }

    @Override
    public List<Attendance> findByUserId(String userId) {
        AttendanceExample example = new AttendanceExample();
        AttendanceExample.Criteria cri = example.createCriteria();
        cri.andUserIdEqualTo(userId);
        return attendanceMapper.selectByExample(example);
    }

    @Override
    public Attendance selectByUserId(String userId) {
        return attendanceMapper.selectByUserId(userId);
    }

    @Override
    public int updateByExample(Attendance attendance) {
        AttendanceExample example = new AttendanceExample();
        AttendanceExample.Criteria cri = example.createCriteria();
        cri.andApproverIdEqualTo(attendance.getApproverId());
        cri.andStatusEqualTo(attendance.getStatus());
        return attendanceMapper.updateByExample(attendance,example);
    }
}
