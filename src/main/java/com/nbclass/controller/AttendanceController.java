package com.nbclass.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nbclass.example.AttendanceExample;
import com.nbclass.model.Attendance;
import com.nbclass.model.User;
import com.nbclass.service.AttendanceService;
import com.nbclass.service.UserService;
import com.nbclass.util.ResultUtil;
import com.nbclass.vo.AttendanceVo;
import com.nbclass.vo.base.PageResultVo;
import com.nbclass.vo.base.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "考勤接口")
@RestController
@RequestMapping("/attendance")
public class AttendanceController {
    private static final Logger logger = LoggerFactory.getLogger(AttendanceController.class);

    @Autowired
    private AttendanceService attendanceService;
    @Autowired
    private UserService userService;


    /**
     * 根据userId查询用户的请假记录
     * @param userId
     * @return
     */
    @ApiOperation(value = "用户请假列表数据", notes = "根据userId查询请假列表数据")
    @PostMapping("/edit")
    @RequiresPermissions("attendance:edit")
    public ResponseVo findByUserId(String userId){
        List<AttendanceVo> attendanceVoList = new ArrayList<>();
        Attendance attendance = attendanceService.selectByUserId(userId);
        User user1 = userService.selectByUserId(userId);
        User user2 = userService.selectByUserId(attendance.getApproverId());
        String username = user1.getUsername();
        String approvername = user2.getUsername();
        AttendanceVo vo = new AttendanceVo();
        vo.setId(attendance.getId());
        vo.setUserId(userId);
        vo.setUsername(username);
        vo.setAttendanceId(attendance.getAttendanceId());
        vo.setAttendanceType(attendance.getAttendanceType());
        vo.setReason(attendance.getReason());
        vo.setApproverId(attendance.getApproverId());
        vo.setApproverName(approvername);
        vo.setStartDate(attendance.getStartDate());
        vo.setEndDate(attendance.getEndDate());
        vo.setStatus(attendance.getStatus());
        attendanceVoList.add(vo);
        return ResultUtil.success("用户请假列表查询成功",attendanceVoList);
    }


    /**
     *  根据条件模糊查询
     * @param attendance
     * @param pageNum
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "请假列表数据", notes = "可按条件(startTime,endTime,attendanceType,status)查询请假列表数据")
    @PostMapping("/list")
    @RequiresPermissions("attendance:list")
    public PageResultVo pageAttendances(Attendance attendance, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "0") Integer pageSize){
        List<AttendanceVo> attendanceVoList = new ArrayList<>();
        PageHelper.startPage(pageNum, pageSize);
        List<Attendance> attendanceLists = attendanceService.selectByAttendance(attendance);
        for (Attendance attendanceList:attendanceLists){
            User user1 = userService.selectByUserId(attendanceList.getUserId());
            User user2 = userService.selectByUserId(attendanceList.getApproverId());
            AttendanceVo vo = new AttendanceVo();
            vo.setId(attendanceList.getId());
            vo.setUserId(attendanceList.getUserId());
            vo.setUsername(user1.getUsername());
            vo.setAttendanceId(attendanceList.getAttendanceId());
            vo.setAttendanceType(attendanceList.getAttendanceType());
            vo.setReason(attendanceList.getReason());
            vo.setApproverId(attendanceList.getApproverId());
            vo.setApproverName(user2.getUsername());
            vo.setStatus(attendanceList.getStatus());
            vo.setStartDate(attendanceList.getStartDate());
            vo.setEndDate(attendanceList.getEndDate());
            attendanceVoList.add(vo);
        }
        PageInfo<AttendanceVo> pages = new PageInfo<>(attendanceVoList);

        return ResultUtil.successTable("用户请假列表查询成功",attendanceVoList,pages.getTotal());
    }

    @ApiOperation(value = "请假申请", notes = "根据attendance添加请假申请")
    @PostMapping("/add")
    @RequiresPermissions("attendance:add")
    public ResponseVo insert(Attendance attendance){
        try {
            int a = attendanceService.insert(attendance);
            if (a == 0) {
                return ResultUtil.success("申请成功");
            } else {
                return ResultUtil.error("申请失败");
            }
        } catch (Exception e) {
            logger.error(String.format("AttendanceController.insert%s", e));
            throw e;
        }
    }

    @ApiOperation(value="撤回请假申请",notes = "根据attendanceId撤回请假申请")
    @PostMapping("/delete")
    @RequiresPermissions("attendance:delete")
    public ResponseVo delete(String attendanceId){
        AttendanceExample example = new AttendanceExample();
        AttendanceExample.Criteria cri = example.createCriteria();
        cri.andAttendanceIdEqualTo(attendanceId);
        int a = attendanceService.deleteByAttendanceId(example);
        if(a == 0){
            return ResultUtil.success("撤回成功");
        }
        return ResultUtil.error("撤回失败");
    }

    @ApiOperation(value = "审批请假",notes = "更新审批状态status，传入approverId")
    @PostMapping("/approve")
    @RequiresPermissions("attendance:approve")
    public ResponseVo approve(String approverId,Integer status){
        Attendance attendance = new Attendance();
        attendance.setApproverId(approverId);
        attendance.setStatus(status);
        int a = attendanceService.updateByExample(attendance);
        if (a > 0){
            return ResultUtil.success("审批成功");
        }
        return ResultUtil.error("审批失败");

    }

}
