/*
Navicat MySQL Data Transfer

Source Server         : lam
Source Server Version : 50645
Source Host           : 120.78.90.101:3306
Source Database       : dormitory

Target Server Type    : MYSQL
Target Server Version : 50645
File Encoding         : 65001

Date: 2020-02-20 21:52:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `attendance`
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `attendance_id` varchar(20) NOT NULL COMMENT '请假id',
  `attendance_type` varchar(20) DEFAULT NULL COMMENT '请假类型',
  `reason` varchar(255) DEFAULT NULL COMMENT '请假原因',
  `approver_id` varchar(20) NOT NULL COMMENT '审批人id',
  `status` int(1) NOT NULL COMMENT '审批状态',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attendance
-- ----------------------------
INSERT INTO `attendance` VALUES ('5', '1000001060757894', '1', '1', null, '1000000461525578', '1', '2019-12-05 00:00:00', '2019-12-06 00:00:00');
INSERT INTO `attendance` VALUES ('6', '1000000499979928', '1', '2', null, '1000000461525578', '0', '2019-12-06 00:00:00', '2019-12-07 00:00:00');
INSERT INTO `attendance` VALUES ('8', '1000001060757894', '1', null, null, '1000000461525578', '1', '2019-12-05 00:00:00', '2019-12-05 00:00:00');
INSERT INTO `attendance` VALUES ('9', '1000001060757894', '1', null, null, '1000000461525578', '1', '2019-12-06 00:00:00', '2019-12-06 00:00:00');
INSERT INTO `attendance` VALUES ('10', '1000001060757894', '1', null, null, '1000000461525578', '1', '2019-12-05 00:00:00', '2019-12-09 15:05:43');
INSERT INTO `attendance` VALUES ('11', '1000001060757894', '1', null, null, '1000000461525578', '1', '2019-12-05 08:30:00', '2019-12-05 12:30:00');

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` varchar(20) NOT NULL COMMENT '角色id',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级权限id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('2', '1000001662405138', '人事部', '人事部', '1', null, '2019-11-21 22:20:09', '2019-11-21 22:20:09');
INSERT INTO `department` VALUES ('3', '1000001424551769', '研发部', '研发部', '1', null, '2019-11-21 23:33:51', '2019-11-21 23:33:51');
INSERT INTO `department` VALUES ('5', '1000001820665141', '市场部', '市场部', '1', null, '2019-11-22 15:11:41', '2019-11-22 15:11:41');

-- ----------------------------
-- Table structure for `permission`
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` varchar(20) NOT NULL COMMENT '权限id',
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) DEFAULT NULL COMMENT '权限访问路径',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `parent_id` int(255) DEFAULT NULL COMMENT '父级权限id',
  `type` int(1) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `order_num` int(3) DEFAULT '0' COMMENT '排序',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '1', '工作台', '工作台', '/workdest', 'workdest', '0', '0', '6', 'fa fa-home', '1', '2017-09-27 21:22:02', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('2', '2', '权限管理', '权限管理', '', null, '0', '0', '5', 'fa fa-th-list', '0', '2017-07-13 15:04:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('3', '201', '用户管理', '用户管理', '/userList', 'users', '0', '0', '3', 'fa fa-circle-o', '1', '2017-07-13 15:05:47', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('4', '20101', '用户列表', '用户列表查询', '/userList', 'user:list', '3', '2', null, null, '1', '2017-07-13 15:09:24', '2019-11-22 15:36:01');
INSERT INTO `permission` VALUES ('5', '20102', '新增用户', '新增用户', '/user/add', 'user:add', '3', '3', '0', null, '1', '2017-07-13 15:06:50', '2018-02-28 17:58:46');
INSERT INTO `permission` VALUES ('6', '20103', '编辑用户', '编辑用户', '/user/edit', 'user:edit', '3', '3', '0', null, '1', '2017-07-13 15:08:03', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('7', '20104', '删除用户', '删除用户', '/user/delete', 'user:delete', '3', '3', '0', null, '1', '2017-07-13 15:08:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('8', '20105', '批量删除用户', '批量删除用户', '/user/batch/delete', 'user:batchDelete', '3', '3', '0', '', '1', '2018-07-11 01:53:09', '2018-07-11 01:53:09');
INSERT INTO `permission` VALUES ('9', '20106', '分配角色', '分配角色', '/user/assign/role', 'user:assignRole', '3', '3', '0', null, '1', '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES ('10', '202', '角色管理', '角色管理', '/roleList', 'roles', '0', '0', '4', 'fa fa-circle-o', '1', '2017-07-17 14:39:09', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('11', '20201', '角色列表', '角色列表查询', '/roleList', 'role:list', '10', '2', null, null, '1', '2017-10-10 15:31:36', '2019-11-22 15:34:35');
INSERT INTO `permission` VALUES ('12', '20202', '新增角色', '新增角色', '/role/add', 'role:add', '10', '3', '0', null, '1', '2017-07-17 14:39:46', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('13', '20203', '编辑角色', '编辑角色', '/role/edit', 'role:edit', '10', '3', '0', null, '1', '2017-07-17 14:40:15', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('14', '20204', '删除角色', '删除角色', '/role/delete', 'role:delete', '10', '3', '0', null, '1', '2017-07-17 14:40:57', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('15', '20205', '批量删除角色', '批量删除角色', '/role/batch/delete', 'role:batchDelete', '10', '3', '0', '', '1', '2018-07-10 22:20:43', '2018-07-10 22:20:43');
INSERT INTO `permission` VALUES ('16', '20206', '分配权限', '分配权限', '/role/assign/permission', 'role:assignPerms', '10', '3', '0', null, '1', '2017-09-26 07:33:05', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('17', '203', '资源管理', '资源管理', '/permissionsList', 'permission:list', '0', '0', '5', 'fa fa-circle-o', '1', '2017-09-26 07:33:51', '2019-11-22 15:36:31');
INSERT INTO `permission` VALUES ('18', '20301', '资源列表', '资源列表', '/permissionsList', 'permission:list', '17', '2', '0', null, '1', '2018-07-12 16:25:28', '2018-07-12 16:25:33');
INSERT INTO `permission` VALUES ('19', '20302', '新增资源', '新增资源', '/permission/add', 'permission:add', '17', '3', '0', null, '1', '2017-09-26 08:06:58', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20', '20303', '编辑资源', '编辑资源', '/permission/edit', 'permission:edit', '17', '3', null, null, '1', '2017-09-27 21:29:04', '2019-11-20 20:08:16');
INSERT INTO `permission` VALUES ('21', '20304', '删除资源', '删除资源', '/permission/delete', 'permission:delete', '17', '3', '0', null, '1', '2017-09-27 21:29:50', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('22', '3', '运维管理', '运维管理', '', null, '0', '0', '3', 'fa fa-th-list', '0', '2018-07-06 15:19:26', '2018-07-06 15:19:26');
INSERT INTO `permission` VALUES ('23', '301', '数据监控', '数据监控', '/database/monitoring', 'database', '22', '1', '1', 'fa fa-circle-o', '0', '2018-07-06 15:19:55', '2018-09-12 13:14:48');
INSERT INTO `permission` VALUES ('24', '4', '系统工具', '系统工具', '', null, '0', '0', '4', 'fa fa-th-list', '0', '2018-07-06 15:20:38', '2018-07-06 15:20:38');
INSERT INTO `permission` VALUES ('25', '401', '图标工具', '图标工具', '/icons', 'icons', '24', '1', '1', 'fa fa-circle-o', '0', '2018-07-06 15:21:00', '2018-07-06 15:21:00');
INSERT INTO `permission` VALUES ('28', '1000000884924014', '在线用户', '在线用户', '/online/users', 'onlineUsers', '2', '1', '4', 'fa fa-circle-o', '0', '2018-07-18 21:00:38', '2018-07-19 12:47:42');
INSERT INTO `permission` VALUES ('29', '1000000433323073', '在线用户查询', '在线用户查询', '/online/user/list', 'onlineUser:list', '28', '2', '0', null, '0', '2018-07-18 21:01:25', '2018-07-19 12:48:04');
INSERT INTO `permission` VALUES ('30', '1000000903407910', '踢出用户', '踢出用户', '/online/user/kickout', 'onlineUser:kickout', '28', '2', '0', null, '0', '2018-07-18 21:41:54', '2018-07-19 12:48:25');
INSERT INTO `permission` VALUES ('31', '1000000851815490', '批量踢出', '批量踢出', '/online/user/batch/kickout', 'onlineUser:batchKickout', '28', '2', '0', '', '0', '2018-07-19 12:49:30', '2018-07-19 12:49:30');
INSERT INTO `permission` VALUES ('38', '1000001938249361', '部门管理', '部门管理', '/departmentList', null, '0', '0', '2', null, '1', '2019-11-20 22:40:41', '2019-11-20 22:41:09');
INSERT INTO `permission` VALUES ('40', '1000000482315783', '新增部门', '新增部门', '/department/add', 'department:add', '38', '3', '0', null, '1', '2019-11-21 20:26:26', '2019-11-21 20:26:26');
INSERT INTO `permission` VALUES ('41', '1000000869087043', '分配部门', '分配部门', '/department/assign/department', 'department:assignDepartment', '38', '3', '0', null, '1', '2019-11-21 20:27:46', '2019-11-21 20:27:46');
INSERT INTO `permission` VALUES ('42', '1000000445085015', '删除部门', '删除部门', '/department/delete', 'department:delete', '38', '3', '0', null, '1', '2019-11-21 20:28:23', '2019-11-21 20:28:23');
INSERT INTO `permission` VALUES ('43', '1000000131770368', '部门详情', '部门详情', '/department/detail', 'department:detail', '38', '3', '0', null, '1', '2019-11-21 20:28:51', '2019-11-21 20:28:51');
INSERT INTO `permission` VALUES ('44', '1000000367701131', '编辑部门', '编辑部门', '/department/edit', 'department:edit', '38', '3', '0', null, '1', '2019-11-21 20:29:17', '2019-11-21 20:29:17');
INSERT INTO `permission` VALUES ('45', '1000001872792983', '部门列表', '部门列表', '/departmentList', 'department:list', '38', '2', '0', null, '1', '2019-11-21 20:29:47', '2019-11-21 20:29:47');
INSERT INTO `permission` VALUES ('57', '1000001276164855', '测试路由', null, '/test', null, '0', '0', '10', null, '0', '2019-11-23 10:47:13', '2019-11-23 10:56:19');
INSERT INTO `permission` VALUES ('58', '1000000889944293', '测试权限', '测试权限', '/test', null, '0', '0', '11', null, '0', '2019-11-23 11:43:09', '2019-12-04 14:32:48');
INSERT INTO `permission` VALUES ('59', '1000000771309099', '考勤管理', '考勤管理', '/attendance', null, '0', '0', '7', null, '1', '2019-12-04 14:23:13', '2019-12-04 16:10:48');
INSERT INTO `permission` VALUES ('60', '1000000253519642', '请假撤回', '请假撤回', '/attendance/delete', 'attendance:delete', '59', '3', '0', null, '1', '2019-12-04 14:26:54', '2019-12-04 14:55:26');
INSERT INTO `permission` VALUES ('61', '1000001520349044', '测试路由', null, '/test', null, '0', '0', '11', null, '1', '2019-12-04 14:52:44', '2019-12-04 14:52:44');
INSERT INTO `permission` VALUES ('62', '1000001664245539', '测试子路由', null, '/test', null, '61', '2', '1', null, '1', '2019-12-04 14:54:03', '2019-12-04 16:13:12');
INSERT INTO `permission` VALUES ('63', '1000000367536322', '个人请假信息', '个人请假信息', '/attendance/edit', 'attendance:edit', '59', '3', '0', null, '1', '2019-12-04 14:56:34', '2019-12-04 16:23:30');
INSERT INTO `permission` VALUES ('64', '1000001454680573', '请假列表', '请假列表', '/attendanceList', 'attendance:list', '59', '2', null, null, '1', '2019-12-04 16:26:26', '2019-12-09 10:34:09');
INSERT INTO `permission` VALUES ('65', '1000001519768864', '请假申请', '请假申请', '/attendance/add', 'attendance:add', '59', '3', '0', null, '1', '2019-12-05 10:51:26', '2019-12-05 10:51:26');
INSERT INTO `permission` VALUES ('66', '1000000554876731', '测试4', '/test', '/test', 'user:AddItem', '0', '0', '0', null, '1', '2019-12-05 10:58:35', '2019-12-05 10:58:35');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '1', '超级管理员', '超级管理员', '1', '2017-06-28 20:30:05', '2017-06-28 20:30:10');
INSERT INTO `role` VALUES ('2', '2', '管理员', '管理员', '1', '2017-06-30 23:35:19', '2017-10-11 09:32:33');
INSERT INTO `role` VALUES ('3', '3', '普通用户', '普通用户', '1', '2017-06-30 23:35:44', '2019-11-20 11:20:26');
INSERT INTO `role` VALUES ('4', '4', '数据库管理员', '数据库管理员', '1', '2017-07-12 11:50:22', '2017-10-09 17:38:02');
INSERT INTO `role` VALUES ('12', '1000001682346096', 'pk', '测试', '0', '2019-11-20 11:20:57', '2019-11-23 11:07:48');
INSERT INTO `role` VALUES ('13', '1000001757634674', '部门管理员', '部门管理员', '1', '2019-11-20 20:10:25', '2019-11-20 20:10:42');
INSERT INTO `role` VALUES ('14', '1000001849026909', '超级管理员备份', '超级管理员备份', '1', '2019-11-21 20:07:36', null);
INSERT INTO `role` VALUES ('15', '1000002050538594', '测试角色', '测试角色', '0', '2019-11-22 14:57:14', '2019-11-22 14:57:19');
INSERT INTO `role` VALUES ('16', '1000001257142511', '测试角色', '测试角色', '0', '2019-11-22 14:57:24', '2019-11-22 23:21:33');

-- ----------------------------
-- Table structure for `role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `permission_id` varchar(20) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2761 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('877', '3', '2');
INSERT INTO `role_permission` VALUES ('878', '3', '201');
INSERT INTO `role_permission` VALUES ('879', '3', '20101');
INSERT INTO `role_permission` VALUES ('880', '3', '20102');
INSERT INTO `role_permission` VALUES ('881', '3', '20103');
INSERT INTO `role_permission` VALUES ('882', '3', '20104');
INSERT INTO `role_permission` VALUES ('883', '3', '20105');
INSERT INTO `role_permission` VALUES ('884', '3', '20106');
INSERT INTO `role_permission` VALUES ('885', '3', '202');
INSERT INTO `role_permission` VALUES ('886', '3', '20201');
INSERT INTO `role_permission` VALUES ('887', '3', '20202');
INSERT INTO `role_permission` VALUES ('888', '3', '20203');
INSERT INTO `role_permission` VALUES ('889', '3', '20204');
INSERT INTO `role_permission` VALUES ('890', '3', '20205');
INSERT INTO `role_permission` VALUES ('891', '3', '20206');
INSERT INTO `role_permission` VALUES ('1427', '1000001849026909', '20206');
INSERT INTO `role_permission` VALUES ('1428', '1000001849026909', '1');
INSERT INTO `role_permission` VALUES ('1429', '1000001849026909', '2');
INSERT INTO `role_permission` VALUES ('1430', '1000001849026909', '201');
INSERT INTO `role_permission` VALUES ('1431', '1000001849026909', '20101');
INSERT INTO `role_permission` VALUES ('1432', '1000001849026909', '20102');
INSERT INTO `role_permission` VALUES ('1433', '1000001849026909', '20103');
INSERT INTO `role_permission` VALUES ('1434', '1000001849026909', '20104');
INSERT INTO `role_permission` VALUES ('1435', '1000001849026909', '20105');
INSERT INTO `role_permission` VALUES ('1436', '1000001849026909', '20106');
INSERT INTO `role_permission` VALUES ('1437', '1000001849026909', '202');
INSERT INTO `role_permission` VALUES ('1438', '1000001849026909', '20201');
INSERT INTO `role_permission` VALUES ('1439', '1000001849026909', '20202');
INSERT INTO `role_permission` VALUES ('1440', '1000001849026909', '20203');
INSERT INTO `role_permission` VALUES ('1441', '1000001849026909', '20204');
INSERT INTO `role_permission` VALUES ('1442', '1000001849026909', '20205');
INSERT INTO `role_permission` VALUES ('1443', '1000001849026909', '20206');
INSERT INTO `role_permission` VALUES ('1444', '1000001849026909', '203');
INSERT INTO `role_permission` VALUES ('1445', '1000001849026909', '20301');
INSERT INTO `role_permission` VALUES ('1446', '1000001849026909', '20302');
INSERT INTO `role_permission` VALUES ('1447', '1000001849026909', '20303');
INSERT INTO `role_permission` VALUES ('1448', '1000001849026909', '20304');
INSERT INTO `role_permission` VALUES ('1449', '1000001849026909', '3');
INSERT INTO `role_permission` VALUES ('1450', '1000001849026909', '301');
INSERT INTO `role_permission` VALUES ('1451', '1000001849026909', '4');
INSERT INTO `role_permission` VALUES ('1452', '1000001849026909', '401');
INSERT INTO `role_permission` VALUES ('1802', '1000001257142511', '1000001872792983');
INSERT INTO `role_permission` VALUES ('1803', '1000001257142511', '201');
INSERT INTO `role_permission` VALUES ('1804', '1000001257142511', '20101');
INSERT INTO `role_permission` VALUES ('1805', '1000001257142511', '20102');
INSERT INTO `role_permission` VALUES ('1806', '1000001257142511', '20103');
INSERT INTO `role_permission` VALUES ('1807', '1000001257142511', '20104');
INSERT INTO `role_permission` VALUES ('1844', '1000001682346096', '20101');
INSERT INTO `role_permission` VALUES ('1845', '1000001682346096', '20102');
INSERT INTO `role_permission` VALUES ('1846', '1000001682346096', '20103');
INSERT INTO `role_permission` VALUES ('1847', '1000001682346096', '20104');
INSERT INTO `role_permission` VALUES ('1848', '1000001682346096', '201');
INSERT INTO `role_permission` VALUES ('1849', '1000001682346096', '202');
INSERT INTO `role_permission` VALUES ('1850', '1000001682346096', '203');
INSERT INTO `role_permission` VALUES ('1851', '1000001682346096', '1');
INSERT INTO `role_permission` VALUES ('2027', '1000001757634674', '20101');
INSERT INTO `role_permission` VALUES ('2028', '1000001757634674', '20206');
INSERT INTO `role_permission` VALUES ('2029', '1000001757634674', '1000000482315783');
INSERT INTO `role_permission` VALUES ('2030', '1000001757634674', '1000000869087043');
INSERT INTO `role_permission` VALUES ('2031', '1000001757634674', '1000000445085015');
INSERT INTO `role_permission` VALUES ('2032', '1000001757634674', '1000000131770368');
INSERT INTO `role_permission` VALUES ('2033', '1000001757634674', '1000000367701131');
INSERT INTO `role_permission` VALUES ('2034', '1000001757634674', '1000001872792983');
INSERT INTO `role_permission` VALUES ('2035', '1000001757634674', '1000001938249361');
INSERT INTO `role_permission` VALUES ('2036', '1000001757634674', '201');
INSERT INTO `role_permission` VALUES ('2037', '1000001757634674', '1000000889944293');
INSERT INTO `role_permission` VALUES ('2728', '1', '20101');
INSERT INTO `role_permission` VALUES ('2729', '1', '20201');
INSERT INTO `role_permission` VALUES ('2730', '1', '20303');
INSERT INTO `role_permission` VALUES ('2731', '1', '20102');
INSERT INTO `role_permission` VALUES ('2732', '1', '20103');
INSERT INTO `role_permission` VALUES ('2733', '1', '20104');
INSERT INTO `role_permission` VALUES ('2734', '1', '20105');
INSERT INTO `role_permission` VALUES ('2735', '1', '20106');
INSERT INTO `role_permission` VALUES ('2736', '1', '20202');
INSERT INTO `role_permission` VALUES ('2737', '1', '20203');
INSERT INTO `role_permission` VALUES ('2738', '1', '20204');
INSERT INTO `role_permission` VALUES ('2739', '1', '20205');
INSERT INTO `role_permission` VALUES ('2740', '1', '20206');
INSERT INTO `role_permission` VALUES ('2741', '1', '20206');
INSERT INTO `role_permission` VALUES ('2742', '1', '20301');
INSERT INTO `role_permission` VALUES ('2743', '1', '20302');
INSERT INTO `role_permission` VALUES ('2744', '1', '20304');
INSERT INTO `role_permission` VALUES ('2745', '1', '1000000482315783');
INSERT INTO `role_permission` VALUES ('2746', '1', '1000000869087043');
INSERT INTO `role_permission` VALUES ('2747', '1', '1000000445085015');
INSERT INTO `role_permission` VALUES ('2748', '1', '1000000131770368');
INSERT INTO `role_permission` VALUES ('2749', '1', '1000000367701131');
INSERT INTO `role_permission` VALUES ('2750', '1', '1000001872792983');
INSERT INTO `role_permission` VALUES ('2751', '1', '1000000253519642');
INSERT INTO `role_permission` VALUES ('2752', '1', '1000000367536322');
INSERT INTO `role_permission` VALUES ('2753', '1', '1000001454680573');
INSERT INTO `role_permission` VALUES ('2754', '1', '1000001938249361');
INSERT INTO `role_permission` VALUES ('2755', '1', '201');
INSERT INTO `role_permission` VALUES ('2756', '1', '202');
INSERT INTO `role_permission` VALUES ('2757', '1', '203');
INSERT INTO `role_permission` VALUES ('2758', '1', '1');
INSERT INTO `role_permission` VALUES ('2759', '1', '1000000771309099');
INSERT INTO `role_permission` VALUES ('2760', '1', '1000001519768864');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL,
  `salt` varchar(128) DEFAULT NULL COMMENT '加密盐值',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) DEFAULT NULL COMMENT '年龄：1男2女',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `status` int(1) NOT NULL COMMENT '用户状态：1有效; 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1000000461525578', 'admin', 'e1931548c6544bccaadf856e38d8cd85', '044567bf7b514ca6c1b2d84da9f9d6bf', '15217637322@163.com', '15217637322', '1', '22', '1', '2019-11-20 14:21:07', '2019-12-02 17:19:57', '2020-01-07 21:36:41');
INSERT INTO `user` VALUES ('6', '1000001060757894', 'pk', 'c9cbdabc81feddb6bf957215db15194e', '5852bb48d16d40be87d9eacbbc0929f0', '15217637322@163.com', '15217637322', '2', '22', '1', '2019-11-19 22:41:02', '2019-12-02 18:25:19', '2019-12-02 17:40:17');
INSERT INTO `user` VALUES ('9', '1000000499979928', 'adminRe', '5d624d0e38e7b5aac4801e037418fa66', 'ad820842c56dec4d1b3107ac9f7d9210', '15217637322@163.com', '15217637322', '1', '22', '1', '2019-11-20 20:14:41', '2019-11-20 20:14:41', '2019-11-29 14:10:51');
INSERT INTO `user` VALUES ('10', '1000000450458159', '测试用户', '62816c408606d53b310ae40e0b70cae3', 'cb836f3381cc1be86e7911f60d61033f', null, null, '1', null, '0', '2019-11-20 20:16:41', '2019-11-21 21:37:54', '2019-11-20 20:16:41');
INSERT INTO `user` VALUES ('11', '1000001328107093', '测试用户2', '9c45437131c4eeb62e968a63df7e6826', '9c0298b8decf09f4781ea76f5c2788aa', null, null, '1', null, '0', '2019-11-22 15:10:11', '2019-11-22 23:23:28', '2019-11-22 15:10:11');
INSERT INTO `user` VALUES ('12', '1000000133182286', '彭康', '5f2c54082f29509185ccac03615fdfbc', '28851966a59541c29fd45abf2813246b', '1521763@163.com', '15217637322', '1', '20', '1', '2019-11-23 11:06:17', '2019-11-23 11:06:17', '2019-11-23 11:48:12');
INSERT INTO `user` VALUES ('13', '1000000032511617', 'lam', 'eb51b0dd5cff2943d4610115f1f53c7a', '5abc6b161b1861886583a5d9429c314d', null, null, null, null, '1', '2019-12-22 17:04:39', '2019-12-22 17:04:39', '2019-12-22 17:04:39');
INSERT INTO `user` VALUES ('14', '1000001235028925', 'lam1', 'b50eaf537415de31074b0971513d2978', '02c1b92b1c139c1d890d062fdca1b3ab', null, null, null, null, '1', '2019-12-22 17:08:57', '2019-12-22 17:08:57', '2019-12-22 17:08:57');

-- ----------------------------
-- Table structure for `user_department`
-- ----------------------------
DROP TABLE IF EXISTS `user_department`;
CREATE TABLE `user_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `department_id` varchar(20) NOT NULL COMMENT '部门id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_department
-- ----------------------------
INSERT INTO `user_department` VALUES ('29', '1000001060757894', '1000001820665141');
INSERT INTO `user_department` VALUES ('30', '1000000461525578', '1000001662405138');
INSERT INTO `user_department` VALUES ('31', '1000000133182286', '1000001662405138');

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('11', '1', '1');
INSERT INTO `user_role` VALUES ('14', '1000001979536131', '1000001682346096');
INSERT INTO `user_role` VALUES ('19', '1000000499979928', '1000001849026909');
INSERT INTO `user_role` VALUES ('25', '1000000133182286', '1000001757634674');
INSERT INTO `user_role` VALUES ('27', '1000001060757894', '1');
INSERT INTO `user_role` VALUES ('30', '1000000461525578', '1');
